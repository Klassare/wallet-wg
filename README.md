# wallet-wg

**Note to Reader**: *This document is an early stage draft intended spurn discussion by the prospective Wallet Working Group members. The content provided below is offered as a starting point and expected to undergo revision. If this document makes a statement or takes an approach, you disagree with, or you can offer a better plan, we want you to provide your input. The goal is to cultivate a working group that shares ideas and reaches those ideas by way of discussion and debate. Critical feedback is crucial in achieving a good result.*

# Tezos Wallet Working Group Proposal

## Mission

To assist wallet developers in producing the best Tezos wallet in the blockchain ecosystem.

The Working Group sets out to provide wallet developers with:

* A catalogue of baseline functionality that is expected of a Tezos wallet. Functionality is expressed in the form of a User Story and contextualized with a functional description.
* Details on Technical Considerations and known common pitfalls.
* Suggestions on UX Design patterns that are successful, and troublesome patterns.

**note**: Everything in this document is meant only as a suggestion and starting point to see if we can find agreement on what to document and how to maintain it to keep it relevant and useful

## Baseline Features

### Wallet Generation / Import

#### Key Creation

##### Create new (hot) wallet

As a user who does not have an existing Tezos wallet or address, I want to generate a new wallet so that I can receive tokens from a third-party

* **Technical Considerations**
    * 160-256 bits entropy (preferably 256) based on cryptographically secure randomness. BIP39 mnemonic phrase for recovery. Enforce a high bit strength for the password if an encrypted key store is created. For symmetric encryption, AES GCM is often a good choice. Choice KDF, parameters and salt based on the latest recommendations (NIST is a good source here).

* **Best Practices**
    * Discourage use of copy to clipboard for seed words
    * Verify the user has retained the seed words

* **TBD**
    * Verification of seed words via ordering based selection to mitigate keylogger risk
    * Determine if useful to have a consistent approach to documenting and explaining encrypted Keystore formats and usage to users
    * Allow user to specify the target curve (tz1, tz2, tz3)

##### **Import/Restore wallet**

As a user who has a private key in the form of a mnemonic phrase, I want to restore my wallet in the wallet program so that I can use my wallet to manage my Tezos tokens. 

* **Technical Considerations**

    * It should always be possible to import with the mnemonic phrase. Validate with the built-in checksum and sanitize the mnemonic phrase.

* **Best Practices**
    * 
* **TBD**
    * Should the default be to require a tz1 address for proper input validation to lessen user confusion when the unexpected resulting account shows a zero balance. The user would be required to explicitly turn off the validation by effectively confirming, "I don't have my tz1 account address, which I'm restoring."
    * Allow user to specify the target curve (tz1, tz2, tz3)

##### **Activate fundraiser wallet**: Used to activate fundraiser allocations after the KYC/AML has been completed

* **Technical Considerations**
    * An anonymous operation that doesn't require any fee or a valid signature.
* **Best Practices**
    * 
* **TBD**
    * Consider reducing visibility as activation is no longer a high priority and a source of confusion

##### **Delegate**

As a user who has a wallet and some tokens, I want to delegate my tokens so that I can participate in the rewards that are part of the Tezos blockchain

* **Technical Considerations**
    * (Todo) Should describe how wallet devs can reliably source a list of bakers for delegation
* **Best Practices**
    * Display a choice of delegates from an unbiased source rather than a paid or limited list
    * No default delegate, the default should either be 'custom' or it should be randomly selected from the list
    * (Recommended) Wallet should validate and either disallow/warn users when they try to delegate to a baker that won't pay them for non-custodial delegation (e.g. we see people delegating to Coinbase, Binance)
* **TBD**
    * It's possible to undelegate, but having this feature too visible often creates confusion for the users. They might get the impression that they first need to undelegate before they set a new delegate. Tezos uses LPoS and undelegating is not beneficial under normal use cases. If a wallet wants to support undelegation it might be a good idea to let delegation to an empty string result in an undelegation.
    * Warn user in case the baker is `dead` (not paying rewards), `overdelegated` (thus will not pay rewards), or has an extremely high fee
    * Allow multidelegation

##### **Send**

* **Technical Considerations**
    * Operations are first created as a JSON object (link). If the public key hasn't been revealed yet, a reveal operation must come first. Counter and branch can be fetched from the node RPC API. Gas and storage limits can be set more accurately by first simulating the operation. Fees can be set by assuming non full blocks and base it on what would pass the pre-validation in a default node. Another approach is to base the fee on historical data (more future proof). Perform input validation, create the JSON object, forge locally (validate against remote forge if the local forge implementation isn't tested comprehensively), sign (0x03 watermark), preapply and inject. Operations can be sent individually or in a batch.
* **Best Practices**
    * Validate that destination is not a known baker and warn (we sometimes see new users confused and sending their tokens to a baker rather than delegating them)
* **TBD**
    * Display the expected storage cost (burn) in addition to the fee
    * Check if the destination account is allocated. If it's not, warn user about additional costs, ensure there are sufficient funds
    * Handle refused operations (stuck in mempool or refused by the node), 'counter-in-the-past/in-the-future' issue workaround

##### **Originate a manager .tz account**

* **Technical Considerations**
    * Scriptless contracts have been migrated to manager.tz contracts. This contract can be originated and used as an extra account with the same functionality as an implicit account. 
* **Best Practices**
    * Due to costs and user confusion about sending from a manager.tz account, it's better to use implicit accounts.
* **TBD**
    * Help user to solve the issue when he cannot withdraw funds from the manager .tz contract due to the lack of funds, suggest using tezos faucet. Generally, this is applicable to all smart contract interactions especially tokens (typical issue).

##### **Input validation and related error handling**:

* **Technical Considerations**
    * Many errors can be prevented with input validation, dry run and/or preapply. 
* **Best Practices**
    * When an error occurs, good error handling and messages, so the user understands why the action failed. This should ease the burden on support.
* **TBD**
    * Can we standardize on common error messages.

##### **Node/Indexer communication error handling**:

* **Technical Considerations**
* **Best Practices**
    * Inform user of node errors and guide user to solve the problem.
* **TBD**
    * Implement an auto-retry (for reads only) policy before displaying errors to user.
    * Check the node is in sync (head timestamp is up to date) and ensure `chain_id` is valid
    * Implement switching to a fallback node (pool of nodes) in case of connectivity or out-of-sync issues
    * Allow user to manually set the node address

##### **List implicit accounts and balances**:

* **Technical Considerations**
    * Implicit accounts are derived locally in the wallet and the balances fetched from the node RPC.
* **Best Practices**
    * Auto-refresh changes to accounts.
* **TBD**

##### **List originated accounts and balances**

* **Technical Considerations**
    * Fetching originated accounts requires an indexer.
* **Best Practices**
* **TBD**

##### **List sent operations**

* **Technical Considerations**
    * Requires an indexer to retrieve. In addition to transactions, display other operations such as activations, reveals, originations, and delegations. 
* **Best Practices**
* **TBD**
    * Display pending operations (injected but not in chain yet)
    * Display refused operations (stuck in mempool or refused by the node due an error), show visual notification 
    
##### **List received**

* **Technical Considerations**
    * Requires an indexer to retrieve.
* **Best Practices**
* **TBD**
    * (Recommended) Auto refresh changes and display visual notification
    * Push notifications

##### **List active delegations**

* **Technical Considerations**
    * Fetched from the node RPC.
* **Best Practices**
* **TBD**
    * Display current delegation status (time-to-first-reward)

##### **Ledger support (not required but should document implementation notes and best practices)**

* **Technical Considerations**
* **Best Practices**
* **TBD**
    * Resolve or document any issues with large operations.
    * Specify device models expected to be supported

### Upcoming features needing discussion for v2 of this "standard" for more advanced wallets

* Token Support
    * FA2, FA1.2, other?
* HD Wallet
    * BIP44, legacy HD paths
    * Curve-specific impementation details (especially ed25519)
    * Account discovery procedure
* Arbitrary contract calls
* RPC node failover

# How to Contribute

We are interested in having everyone participate in refining the goals of the wallet working group by contributing their ideas to the Mission, Baseline, and Upcoming sections of this current [**readme**.](https://gitlab.com/tezosagora/wallet-wg/-/blob/master/README.md)

Once you have read the current document, please open a merge request with your changes and message the wallet working group in slack to encourage discussion.
